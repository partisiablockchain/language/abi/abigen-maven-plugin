package codegen;

import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Callback;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.serialization.StateAccessor;

@AutoSysContract(StateVoid.class)
public final class CallbackShortnameOutOfBounds {

  @Callback(128)
  public StateVoid callback1() {
    return null;
  }

  @Init
  public StateVoid init() {
  }

  @Upgrade
  public StateVoid upgrade(StateAccessor oldState) {
    return null;
  }
}
