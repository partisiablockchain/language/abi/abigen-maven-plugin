package com.partisiablockchain.language;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abicodegen.CliParser;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

/** Mojo for generating java code from an ABI file using abi-client's AbiCodeGen. */
@Mojo(name = "abi-code-gen", defaultPhase = LifecyclePhase.GENERATE_SOURCES)
public final class AbiCode extends AbstractMojo {

  @Parameter(defaultValue = "${session}", required = true, readonly = true)
  MavenSession session;

  /** Path to abi files to be generated code for. */
  @Parameter String abiPath;

  /** Whether the code should be generated as test sources instead. */
  @Parameter(property = "abi-code-gen.generateTestSources", defaultValue = "false")
  boolean generateTestSources;

  /** Whether code will be generated to deserialize contract state. */
  @Parameter(defaultValue = "true")
  boolean deserializeState;

  /** Whether code will be generated to deserialize rpc. */
  @Parameter(defaultValue = "false")
  boolean deserializeRpc;

  /** Whether code will be generated to serialize actions. */
  @Parameter(defaultValue = "true")
  boolean serializeActions;

  /** The package name for which the generated code will be placed in. */
  @Parameter(defaultValue = "com.partisiablockchain.language.abicodegen")
  String packageName;

  /** Whether code annotation '@AbiGenerated' should be generated. */
  @Parameter(defaultValue = "true")
  boolean generateAnnotations;

  /** A list of additional command line arguments to pass to the abi-code-gen. */
  @Parameter List<String> arguments;

  @Override
  public void execute() throws MojoExecutionException {
    MavenProject project = session.getCurrentProject();

    Path projectFolder = session.getCurrentProject().getBasedir().toPath();
    Path fullAbiPath = projectFolder.resolve(abiPath);

    if (!Files.exists(fullAbiPath)) {
      throw new MojoExecutionException("Abi file does not exist");
    }
    var rootOutputPath =
        project
            .getBasedir()
            .toPath()
            .resolve(
                generateTestSources
                    ? "target/generated-test-sources/generated-abi"
                    : "target/generated-sources/generated-abi");

    var outputPath = rootOutputPath.resolve(packageName.replace(".", "/"));

    var args = new ArrayList<String>();
    args.add("codegen");
    if (deserializeRpc) {
      args.add("--deserialize-rpc");
    }
    if (!deserializeState) {
      args.add("--dont-deserialize-state");
    }
    if (!serializeActions) {
      args.add("--dont-serialize-actions");
    }
    if (!generateAnnotations) {
      args.add("--dont-annotate");
    }
    args.add("--package");
    args.add(packageName);
    if (arguments != null) {
      args.addAll(arguments);
    }

    var abiFile = fullAbiPath.toFile();
    var subFiles = abiFile.listFiles();
    if (subFiles == null) {
      subFiles = new File[] {abiFile};
    }
    var abiSubFiles =
        Arrays.stream(subFiles).filter(f -> f.getName().endsWith(".abi")).toArray(File[]::new);
    if (abiSubFiles.length == 0) {
      throw new MojoExecutionException(
          "Problem with <abiPath>. No ABI file with extension .abi was found.");
    }
    for (var f : abiSubFiles) {
      generateCode(f, outputPath, args);
    }

    if (generateTestSources) {
      project.addTestCompileSourceRoot(rootOutputPath.toString());
    } else {
      project.addCompileSourceRoot(rootOutputPath.toString());
    }
  }

  private static void generateCode(File file, Path outputPath, List<String> args) {
    var actualArgs = new ArrayList<>(args);
    actualArgs.addAll(
        List.of(
            "--java",
            file.toString(),
            outputPath.toString() + "/" + getContractName(file) + ".java"));
    CliParser.main(actualArgs.toArray(new String[0]));
  }

  private static String getContractName(File file) {
    var fileName = file.toPath().getFileName().toString();
    var endIndex = fileName.indexOf(".abi");
    var name = fileName.substring(0, endIndex);
    name =
        Arrays.stream(name.split("_"))
            .map(str -> str.substring(0, 1).toUpperCase(Locale.ROOT) + str.substring(1))
            .collect(Collectors.joining());
    name = name.replace("-", "_");
    name = name.replace(".", "_");
    return name;
  }
}
