package com.partisiablockchain.language;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import org.apache.maven.execution.DefaultMavenExecutionRequest;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Build;
import org.apache.maven.model.Model;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.apache.maven.project.artifact.ProjectArtifact;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

final class AbiCodeTest {

  private MavenProject project;
  private MavenSession session;
  private AbiCode mojo;

  @BeforeEach
  @SuppressWarnings("deprecation")
  void setUp() {
    final var build = new Build();
    build.setDirectory("target");

    final var model = new Model();
    model.setBuild(build);
    model.setVersion("1.0.0");
    model.setGroupId("com.example");
    model.setArtifactId("attachabi-test-artifact");

    project = new MavenProject();
    project.setArtifact(new ProjectArtifact(project));
    project.setModel(model);
    project.setFile(new File("pom.xml").getAbsoluteFile());

    session = new MavenSession(null, new DefaultMavenExecutionRequest(), null, List.of(project));
    mojo = createTestMojo();
  }

  @AfterEach
  void deleteGenerated() {
    var sourcePath = Path.of("target/generated-sources/generated-abi").toFile();
    var testSourcePath = Path.of("target/generated-test-sources/generated-abi").toFile();
    deleteDirectory(sourcePath);
    deleteDirectory(testSourcePath);
  }

  @Test
  void generateCode() throws MojoExecutionException, IOException {
    assertThat(project.getCompileSourceRoots().size()).isEqualTo(0);
    mojo.execute();
    var generatedSourceRoot =
        project.getBasedir().toPath().resolve("target/generated-sources/generated-abi");
    assertThat(Path.of(project.getCompileSourceRoots().get(0))).isEqualTo(generatedSourceRoot);
    var code =
        Files.readString(
            generatedSourceRoot
                .resolve(mojo.packageName.replace(".", "/"))
                .resolve("TokenContract_SDK_11_0_0.java"));
    assertThat(!code.contains("public static TransferRpc deserializeRpc(byte[] bytes) {")).isTrue();
    assertThat(!code.contains("public static Signature deserialize(byte[] bytes) {")).isTrue();
    assertThat(code.contains("public static byte[] transfer(BlockchainAddress to, long value) {"))
        .isTrue();
    assertThat(code.contains("public static TokenContractState deserialize(byte[] bytes) {"))
        .isTrue();
    assertThat(code).contains("@AbiGenerated");
  }

  @Test
  void generateTestCode() throws MojoExecutionException {
    mojo.generateTestSources = true;
    assertThat(project.getTestCompileSourceRoots().size()).isEqualTo(0);
    mojo.execute();
    var generatedSourceRoot =
        project.getBasedir().toPath().resolve("target/generated-test-sources/generated-abi");
    assertThat(Path.of(project.getTestCompileSourceRoots().get(0))).isEqualTo(generatedSourceRoot);
    assertThat(
            generatedSourceRoot
                .resolve(mojo.packageName.replace(".", "/"))
                .resolve("TokenContract_SDK_11_0_0.java")
                .toFile()
                .exists())
        .isTrue();
  }

  @Test
  void generateCodeAbiPathDir() throws MojoExecutionException {
    mojo.abiPath = "src/test/resources/language";
    mojo.execute();
    var generatedSourceRoot =
        project.getBasedir().toPath().resolve("target/generated-sources/generated-abi");
    assertThat(
            generatedSourceRoot
                .resolve(mojo.packageName.replace(".", "/"))
                .resolve("TokenContract_SDK_11_0_0.java")
                .toFile()
                .exists())
        .isTrue();
  }

  @Test
  void generateCodeArgs() throws MojoExecutionException, IOException {
    mojo.deserializeRpc = true;
    mojo.deserializeState = false;
    mojo.serializeActions = false;
    mojo.generateAnnotations = false;
    mojo.arguments = List.of("--deserialize", "Signature");
    mojo.execute();
    var generatedSourceRoot =
        project.getBasedir().toPath().resolve("target/generated-sources/generated-abi");
    var code =
        Files.readString(
            generatedSourceRoot
                .resolve(mojo.packageName.replace(".", "/"))
                .resolve("TokenContract_SDK_11_0_0.java"));
    assertThat(code.contains("public static TransferRpc deserializeRpc(byte[] bytes) {")).isTrue();
    assertThat(code.contains("public static Signature deserialize(byte[] bytes) {")).isTrue();
    assertThat(!code.contains("public static byte[] transfer(BlockchainAddress to, long value) {"))
        .isTrue();
    assertThat(!code.contains("public static TokenContractState deserialize(byte[] bytes) {"))
        .isTrue();
    assertThat(code).doesNotContain("@AbiGenerated");
  }

  @Test
  void generateCodeDirectoryNotExists() {
    mojo.abiPath = "src/does/not/exist";
    assertThatThrownBy(mojo::execute)
        .isInstanceOf(MojoExecutionException.class)
        .hasMessageContaining("Abi file does not exist");
  }

  @Test
  void generateCodeDirWithNoAbi() {
    mojo.abiPath = "src/test/resources/dirNoAbi";
    assertThatThrownBy(mojo::execute)
        .isInstanceOf(MojoExecutionException.class)
        .hasMessageContaining("Problem with <abiPath>. No ABI file with extension .abi was found.");
  }

  @Test
  void generateCodeWrongFileExtension() {
    mojo.abiPath = "src/test/resources/dirNoAbi/text.txt";
    assertThatThrownBy(mojo::execute)
        .isInstanceOf(MojoExecutionException.class)
        .hasMessageContaining("Problem with <abiPath>. No ABI file with extension .abi was found.");
  }

  private AbiCode createTestMojo() {
    var mojo = new AbiCode();
    mojo.session = session;
    mojo.abiPath = "src/test/resources/language/token_contract-SDK-11.0.0.abi";
    mojo.generateTestSources = false;
    mojo.serializeActions = true;
    mojo.deserializeState = true;
    mojo.deserializeRpc = false;
    mojo.generateAnnotations = true;
    mojo.packageName = "com.partisiablockchain.language.abicodegen";
    return mojo;
  }

  private static void deleteDirectory(File dir) {
    File[] contents = dir.listFiles();
    if (contents != null) {
      for (File file : contents) {
        deleteDirectory(file);
      }
    }
    dir.delete();
  }
}
