package com.partisiablockchain.language;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.parser.FnKind;
import com.partisiablockchain.language.abiclient.types.ArgumentAbi;
import com.partisiablockchain.language.abiclient.types.FnAbi;
import com.partisiablockchain.language.abiclient.types.NamedTypeSpec;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * A class containing type hints for bridging the gap between the rich Java types and the
 * comparatively simple ABI types.
 */
public final class CodegenTypeHint {

  /** Maps actions/callbacks to the signature of said function. */
  private final Map<FunctionKey, MethodSignature> functionSignatures = new LinkedHashMap<>();

  private final String contractSimpleName;
  private final String packageName;
  private final String stateSimpleName;
  /** Maps ABI name of structs/enums to the fully-qualified Java name. */
  private final Map<String, String> abiNameToQualifiedName;
  /** A list of names for types that are structs or enums. */
  private final List<String> structOrEnumNames;

  private UpgradeSignature upgradeSignature;

  /**
   * Create a new type hint object.
   *
   * @param contractSimpleName the simple name of the contract.
   * @param packageName the package of the contract
   * @param stateSimpleName the simple name of the state class
   * @param abiNameToQualifiedName a map of {@link NamedTypeSpec} name to fully qualified class name
   * @param structOrEnumNames a list of names for types that are structs or enums
   */
  public CodegenTypeHint(
      String contractSimpleName,
      String packageName,
      String stateSimpleName,
      Map<String, String> abiNameToQualifiedName,
      List<String> structOrEnumNames) {
    this.contractSimpleName = contractSimpleName;
    this.packageName = packageName;
    this.stateSimpleName = stateSimpleName;
    this.abiNameToQualifiedName = abiNameToQualifiedName;
    this.structOrEnumNames = structOrEnumNames;
  }

  /**
   * Look up the qualified name of a ABI struct/enum type.
   *
   * @param namedTypeSpecName the struct/enum name in the ABI
   * @return the fully qualified name
   */
  public String lookupQualifiedName(String namedTypeSpecName) {
    return Objects.requireNonNull(abiNameToQualifiedName.get(namedTypeSpecName));
  }

  /**
   * Get the signature for a function.
   *
   * @param function the function to look up
   * @return the method signature for the function
   */
  public MethodSignature getSignature(FnAbi function) {
    var key = new FunctionKey(function.kind(), function.name());
    return functionSignatures.get(key);
  }

  /**
   * The simple name of the contract class.
   *
   * @return the simple name
   */
  public String contractSimpleName() {
    return contractSimpleName;
  }

  /**
   * The package name of the contract.
   *
   * @return the contract package
   */
  public String packageName() {
    return packageName;
  }

  /**
   * Add method signatures for a function.
   *
   * @param kind the function kind
   * @param name the function name
   * @param signature the method signature
   */
  public void addMethodSignatures(FnKind kind, String name, MethodSignature signature) {
    var key = new FunctionKey(kind, name);
    this.functionSignatures.put(key, signature);
  }

  /**
   * Get the signature for the upgrade method.
   *
   * @return the upgrade method signature
   */
  public UpgradeSignature getUpgradeSignature() {
    return upgradeSignature;
  }

  /**
   * Set the signature for the upgrade method.
   *
   * @param signature the signature
   */
  public void setUpgradeSignature(UpgradeSignature signature) {
    this.upgradeSignature = signature;
  }

  /**
   * Get the simple name of the state class.
   *
   * @return the simple name for the state class
   */
  public String stateSimpleName() {
    return stateSimpleName;
  }

  /**
   * Test whether the named type spec is a struct or an enum (not a variant).
   *
   * @param namedType the type to test
   * @return true if it's not a enum variant
   */
  public boolean isStructOrEnum(NamedTypeSpec namedType) {
    return structOrEnumNames.contains(namedType.name());
  }

  /** Type hinted method signature. */
  public static final class MethodSignature {

    /** Whether the represented method has a system context. */
    public boolean sysContext;
    /** Whether the represented method has a callback context. */
    public boolean callbackContext;
    /** Whether the represented method has a contract state. */
    public boolean state;
    /** Whether the represented method has a raw {@link com.secata.stream.SafeDataInputStream}. */
    public boolean rpc;
  }

  /**
   * Composite key of function kind and name.
   *
   * @param kind the function kind
   * @param name the name of the function
   */
  record FunctionKey(FnKind kind, String name) {}

  /**
   * The delegated upgrade method signature.
   *
   * @param name the name of the method
   * @param arguments the arguments of the method
   * @param rpc whether the method has an {@link com.secata.stream.SafeDataInputStream}
   */
  public record UpgradeSignature(String name, List<ArgumentAbi> arguments, boolean rpc) {}
}
