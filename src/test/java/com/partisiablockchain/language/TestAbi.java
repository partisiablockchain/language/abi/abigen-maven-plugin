package com.partisiablockchain.language;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.abiclient.json.AbiJsonMapper;
import com.partisiablockchain.language.abiclient.parser.ContractAbi;
import com.partisiablockchain.language.abiclient.parser.FnKind;
import com.partisiablockchain.language.abiclient.types.AbiVersion;
import com.partisiablockchain.language.abiclient.types.ArgumentAbi;
import com.partisiablockchain.language.abiclient.types.FieldAbi;
import com.partisiablockchain.language.abiclient.types.FileAbi;
import com.partisiablockchain.language.abiclient.types.FnAbi;
import com.partisiablockchain.language.abiclient.types.NamedTypeRef;
import com.partisiablockchain.language.abiclient.types.NamedTypeSpec;
import com.partisiablockchain.language.abiclient.types.SimpleTypeSpec;
import com.partisiablockchain.language.abiclient.types.StructTypeSpec;
import com.partisiablockchain.language.abiclient.types.TypeSpec;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

final class TestAbi {

  static Path createTempJson() {
    try {
      var mapper = new AbiJsonMapper();
      var json = mapper.writeAsString(create());

      Path temp = Files.createTempFile("abi", ".json");
      Path targetFile = Paths.get("target", "tempjson", temp.getFileName().toString());
      Files.createDirectories(targetFile.getParent());

      Files.writeString(targetFile, json);
      return targetFile;
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  static FileAbi create() {
    var namedTypes = new ArrayList<NamedTypeSpec>();
    namedTypes.add(
        new StructTypeSpec(
            "my_struct",
            List.of(new FieldAbi("my_field", new SimpleTypeSpec(TypeSpec.TypeIndex.u64)))));

    var functions = new ArrayList<FnAbi>();
    functions.add(
        new FnAbi(
            FnKind.Init,
            "initialize",
            new byte[1],
            List.of(new ArgumentAbi("arg1", new SimpleTypeSpec(TypeSpec.TypeIndex.i16)))));
    functions.add(new FnAbi(FnKind.Action, "action", new byte[] {0x01}, List.of()));

    var contract = new ContractAbi(namedTypes, functions, new NamedTypeRef(0));
    return new FileAbi("PBCABI", new AbiVersion(8, 0, 0), new AbiVersion(4, 0, 0), null, contract);
  }
}
