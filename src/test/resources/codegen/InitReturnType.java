package codegen;

import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.serialization.StateVoid;
import com.partisiablockchain.serialization.StateAccessor;

@AutoSysContract(StateVoid.class)
public final class InitReturnType {

  @Init
  public void init() {
    return null;
  }

  @Upgrade
  public StateVoid upgrade(StateAccessor oldState) {
    return null;
  }
}
