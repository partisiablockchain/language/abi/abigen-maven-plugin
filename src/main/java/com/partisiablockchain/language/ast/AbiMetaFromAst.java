package com.partisiablockchain.language.ast;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import com.partisiablockchain.language.AbiWithTypeHint;
import com.partisiablockchain.language.CodegenTypeHint;
import com.partisiablockchain.language.abiclient.parser.ContractAbi;
import com.partisiablockchain.language.abiclient.parser.FnKind;
import com.partisiablockchain.language.abiclient.types.AbiVersion;
import com.partisiablockchain.language.abiclient.types.ArgumentAbi;
import com.partisiablockchain.language.abiclient.types.EnumTypeSpec;
import com.partisiablockchain.language.abiclient.types.EnumVariant;
import com.partisiablockchain.language.abiclient.types.FieldAbi;
import com.partisiablockchain.language.abiclient.types.FileAbi;
import com.partisiablockchain.language.abiclient.types.FnAbi;
import com.partisiablockchain.language.abiclient.types.NamedTypeRef;
import com.partisiablockchain.language.abiclient.types.NamedTypeSpec;
import com.partisiablockchain.language.abiclient.types.StructTypeSpec;
import com.partisiablockchain.language.abiclient.types.TypeSpec;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.ExecutableElement;
import javax.lang.model.element.RecordComponentElement;
import javax.lang.model.element.TypeElement;
import javax.lang.model.type.TypeMirror;
import javax.lang.model.util.Types;

/** Generates ABI and java type hinting information from AST methods and types. */
public final class AbiMetaFromAst {

  private static final byte[] SHORTNAME_INIT =
      new byte[] {(byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0xFF, (byte) 0x0F};

  private final Types types;
  private final AstType stateType;

  private final NamedTypeCollector collector;
  private final List<FnAbiWithSignature> actions = new ArrayList<>();
  private FnAbiWithSignature upgradeAction;

  /**
   * Construct the ABI from AST creator.
   *
   * @param types the types utility
   * @param stateType the contract state type mirror
   */
  public AbiMetaFromAst(Types types, TypeMirror stateType) {
    this.types = types;
    this.stateType = createAstType(stateType);

    TypeElement stateTypeElement = this.stateType.asTypeElementUnsafe();
    this.collector = new NamedTypeCollector(stateTypeElement);
  }

  private static List<? extends Element> getEnumConstants(TypeElement element) {
    return element.getEnclosedElements().stream()
        .filter(e -> e.getKind() == ElementKind.ENUM_CONSTANT)
        .toList();
  }

  /**
   * Add the upgrade action.
   *
   * @param method the AST method representing the upgrade method
   */
  public void addUpgrade(ExecutableElement method) {
    // FnKind and shortname is ignored
    this.upgradeAction = buildAbiForMethod(FnKind.Action, new byte[] {-1}, method);
  }

  /**
   * A function ABI with an enriched signature.
   *
   * @param fnAbi the function ABI
   * @param methodSignature the enriched method signature
   */
  @SuppressWarnings("unused")
  private record FnAbiWithSignature(FnAbi fnAbi, CodegenTypeHint.MethodSignature methodSignature) {}

  /**
   * Add a list of contract actions.
   *
   * @param actions the actions to process
   */
  public void addActions(List<ActionExecutableElement> actions) {
    for (ActionExecutableElement actionExecutableElement : actions) {
      int shortname = actionExecutableElement.shortname();
      ExecutableElement method = actionExecutableElement.method();

      this.actions.add(buildAbiForMethod(FnKind.Action, shortnameAsBytes(shortname), method));
    }
  }

  /**
   * Add a list of contract callbacks.
   *
   * @param callbacks the callbacks to process
   */
  public void addCallbacks(List<ActionExecutableElement> callbacks) {
    for (ActionExecutableElement actionExecutableElement : callbacks) {
      int shortname = actionExecutableElement.shortname();
      ExecutableElement method = actionExecutableElement.method();

      this.actions.add(buildAbiForMethod(FnKind.Callback, shortnameAsBytes(shortname), method));
    }
  }

  /**
   * Add a contract initializer.
   *
   * @param method the method to process
   */
  public void addInit(ExecutableElement method) {
    this.actions.add(buildAbiForMethod(FnKind.Init, SHORTNAME_INIT, method));
  }

  /**
   * Create a type-hinted ABI.
   *
   * @param packageName the destination package
   * @param contractClassName the contract class name
   * @return the type-hinted ABI
   */
  public AbiWithTypeHint toFileAbi(String packageName, String contractClassName) {
    var typeHint =
        new CodegenTypeHint(
            contractClassName,
            packageName,
            stateType.asTypeElementUnsafe().getSimpleName().toString(),
            collector.abiNameToQualified(),
            collector.getStructOrEnumNames());

    var signature =
        new CodegenTypeHint.UpgradeSignature(
            upgradeAction.fnAbi.name(),
            upgradeAction.fnAbi.arguments(),
            upgradeAction.methodSignature.rpc);
    typeHint.setUpgradeSignature(signature);

    for (FnAbiWithSignature action : actions) {
      FnAbi fnAbi = action.fnAbi();
      typeHint.addMethodSignatures(fnAbi.kind(), fnAbi.name(), action.methodSignature());
    }

    List<NamedTypeSpec> namedTypeSpecs = new ArrayList<>();
    for (Element element : collector.getElements()) {
      ElementKind kind = element.getKind();
      if (kind == ElementKind.RECORD) {
        TypeElement typeElement = (TypeElement) element;
        namedTypeSpecs.add(buildStructAbi(typeElement));
      } else if (kind == ElementKind.ENUM) {
        namedTypeSpecs.add(buildEnumAbi((TypeElement) element));
      } else {
        // Enum constant
        StructTypeSpec structTypeSpec =
            new StructTypeSpec(collector.getAbiName(element), List.of());
        namedTypeSpecs.add(structTypeSpec);
      }
    }

    namedTypeSpecs.add(new StructTypeSpec(collector.getAbiName(stateType), List.of()));

    List<FnAbi> fnAbiList = actions.stream().map(FnAbiWithSignature::fnAbi).toList();
    ContractAbi contractAbi =
        new ContractAbi(namedTypeSpecs, fnAbiList, new NamedTypeRef(namedTypeSpecs.size() - 1));

    FileAbi abi =
        new FileAbi("PBCABI", new AbiVersion(9, 0, 0), new AbiVersion(5, 1, 0), null, contractAbi);

    return new AbiWithTypeHint(abi, typeHint);
  }

  private FnAbiWithSignature buildAbiForMethod(
      FnKind kind, byte[] shortname, ExecutableElement method) {
    List<? extends Element> parameters = method.getParameters();
    List<TypeMirror> parameterTypes = typesFromElements(parameters);

    List<ArgumentAbi> argumentAbis = new ArrayList<>();

    CodegenTypeHint.MethodSignature signature = new CodegenTypeHint.MethodSignature();
    for (int i = 0; i < parameters.size(); i++) {
      Element parameter = parameters.get(i);

      TypeMirror parameterType = parameterTypes.get(i);
      AstType astType = createAstType(parameterType);

      AbiParameterTypeSpecFromAst typeSpecFromAst =
          new AbiParameterTypeSpecFromAst(types, collector);
      if (isUserDefined(astType)) {
        TypeSpec typeSpec = typeSpecFromAst.createSpecFromParameter(parameter, astType);
        argumentAbis.add(new ArgumentAbi(parameter.getSimpleName().toString(), typeSpec));
      }

      signature.sysContext |= astType.isSysContext();
      signature.callbackContext |= astType.isCallbackContext();
      signature.state |= astType.isTypeOf(stateType.getWrapped());
      signature.rpc |= astType.isRpcStream();
    }

    String functionName = method.getSimpleName().toString();
    FnAbi fnAbi = new FnAbi(kind, functionName, shortname, argumentAbis);
    return new FnAbiWithSignature(fnAbi, signature);
  }

  private EnumTypeSpec buildEnumAbi(TypeElement element) {
    List<EnumVariant> variants = new ArrayList<>();
    TypeElement enumeration = (TypeElement) types.asElement(element.asType());

    List<? extends Element> enumConstants = getEnumConstants(enumeration);
    for (int discriminant = 0; discriminant < enumConstants.size(); discriminant++) {
      Element constant = enumConstants.get(discriminant);

      NamedTypeRef ref = new NamedTypeRef(collector.indexOf(constant));
      variants.add(new EnumVariant(discriminant, ref));
    }
    return new EnumTypeSpec(collector.getAbiName(element), variants);
  }

  private StructTypeSpec buildStructAbi(TypeElement element) {
    List<FieldAbi> fields = new ArrayList<>();

    TypeElement record = (TypeElement) types.asElement(element.asType());
    for (RecordComponentElement component : record.getRecordComponents()) {
      TypeMirror parameterType = component.asType();

      AbiParameterTypeSpecFromAst typeSpecFromAst =
          new AbiParameterTypeSpecFromAst(types, collector);
      TypeSpec typeSpec =
          typeSpecFromAst.createSpecFromParameter(component, createAstType(parameterType));

      fields.add(new FieldAbi(component.getSimpleName().toString(), typeSpec));
    }

    String structName = collector.getAbiName(record);
    return new StructTypeSpec(structName, fields);
  }

  /** Disambiguate all registered names. */
  public void disambiguateNames() {
    collector.disambiguate();
  }

  /**
   * Collect the types from the parameters of AST methods.
   *
   * @param methods the methods to look at
   */
  public void collectTypes(Collection<ActionExecutableElement> methods) {
    for (ActionExecutableElement ast : methods) {
      ExecutableElement method = ast.method();
      List<? extends Element> parameters = method.getParameters();
      List<TypeMirror> parameterTypes = typesFromElements(parameters);

      for (int i = 0; i < parameters.size(); i++) {
        TypeMirror parameterType = parameterTypes.get(i);
        collectTypes(createAstType(parameterType));
      }
    }
  }

  void collectTypes(AstType parameterType) {
    if (parameterType.isList()) {
      TypeMirror generic = parameterType.extractGeneric();
      collectTypes(createAstType(generic));
    } else if (parameterType.isRecord()) {
      TypeElement record = parameterType.asTypeElementUnsafe();

      collector.addNamedType(record);

      for (RecordComponentElement component : record.getRecordComponents()) {
        collectTypes(createAstType(component.asType()));
      }
    } else if (parameterType.isEnum()) {
      TypeElement enumElement = parameterType.asTypeElementUnsafe();
      collector.addNamedType(enumElement);

      List<? extends Element> constants = getEnumConstants(enumElement);
      for (Element constant : constants) {
        collector.addNamedType(constant);
      }
    }
  }

  private AstType createAstType(TypeMirror generic) {
    return new AstType(generic, types);
  }

  private boolean isUserDefined(AstType astType) {
    boolean builtInOrState =
        astType.isPartOfRpcSignature() || astType.isTypeOf(stateType.getWrapped());
    return !builtInOrState;
  }

  private static List<TypeMirror> typesFromElements(List<? extends Element> parameters) {
    return parameters.stream().map(Element::asType).toList();
  }

  private static byte[] shortnameAsBytes(int shortname) {
    return new byte[] {(byte) shortname};
  }
}
