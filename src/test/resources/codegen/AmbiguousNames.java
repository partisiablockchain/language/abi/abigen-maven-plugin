package codegen;

import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateVoid;
import java.util.List;

@AutoSysContract(StateVoid.class)
public final class AmbiguousNames {

  record Ambiguity1() {

    record A(int x) {

    }
  }

  record Ambiguity2() {

    record A(int x) {

    }
  }

  @Action(1)
  public StateVoid action1(
      List<Ambiguity1.A> vecOfA1,
      List<Ambiguity2.A> vecOfA2,
      Ambiguity1.A a1
  ) {
    return null;
  }

  @Init
  public StateVoid init(int x, int y) {
    return null;
  }

  @Upgrade
  public StateVoid upgrade(StateAccessor stateAccessor) {
    return null;
  }
}
