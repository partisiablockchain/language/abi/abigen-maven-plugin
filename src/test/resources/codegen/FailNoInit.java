package codegen;

import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.serialization.StateVoid;

@AutoSysContract(StateVoid.class)
public final class FailNoInit {

  @Action(1)
  public StateVoid action1(int i1, long l1) {
    return null;
  }

  @Upgrade
  public StateVoid upgrade(int x, int y) {
    return null;
  }
}
