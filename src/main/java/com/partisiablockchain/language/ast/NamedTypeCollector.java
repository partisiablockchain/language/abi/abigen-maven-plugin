package com.partisiablockchain.language.ast;

/*
 * Copyright (C) 2022 - 2023 Partisia Blockchain Foundation
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import javax.lang.model.element.Element;
import javax.lang.model.element.ElementKind;
import javax.lang.model.element.TypeElement;

/**
 * Handles the mapping of names of types and enum constants to an index in the ABI.
 *
 * <p>All structs, enums and enum constants are added to this class and get a unique index.
 *
 * <p>Java qualified names get transformed into their simplest form. If a collision exists the names
 * are disambiguated so two types a.b.C and a.x.C will become b.C and x.C.
 */
final class NamedTypeCollector {

  /** This is used to keep track of the type indices. */
  private final List<String> elementNames = new ArrayList<>();

  /** A list that tracks which qualified names represent structs or enums. */
  private final List<String> structOrEnumNames = new ArrayList<>();

  /** Map from a fully qualified name to the AST element represented by the name. */
  private final Map<String, Element> elements = new LinkedHashMap<>();

  /** Map from a fully qualified name to the mutable equivalent. */
  private final Map<String, NameWithMutablePackage> identifiers = new HashMap<>();

  NamedTypeCollector(TypeElement stateType) {
    registerIdentifier(stateType, stateType.getQualifiedName().toString());
  }

  /**
   * Add the AST element to the list of known types.
   *
   * @param element the element
   */
  void addNamedType(Element element) {
    int index = indexOf(element);
    if (index == -1) {
      String qualifiedName = getFullyQualifiedName(element);
      ElementKind kind = element.getKind();
      if (kind == ElementKind.RECORD || kind == ElementKind.ENUM) {
        structOrEnumNames.add(qualifiedName);
      }

      registerIdentifier(element, qualifiedName);
    }
  }

  /**
   * Look up the index of the element.
   *
   * @param element the element to look up
   * @return the index of the element or -1 if it does not exist
   */
  int indexOf(Element element) {
    String name = getFullyQualifiedName(element);
    return elementNames.indexOf(name);
  }

  private String getFullyQualifiedName(Element element) {
    ElementKind kind = element.getKind();
    if (kind == ElementKind.RECORD || kind == ElementKind.ENUM) {
      TypeElement type = (TypeElement) element;
      return type.getQualifiedName().toString();
    } else {
      // Enum constant
      return getFullyQualifiedName(element.getEnclosingElement()) + "$" + element.getSimpleName();
    }
  }

  private void registerIdentifier(Element element, String qualifiedName) {
    if (element.getKind() != ElementKind.CLASS) {
      elements.put(qualifiedName, element);
      elementNames.add(qualifiedName);
    }

    List<String> nameComponents = splitToString(qualifiedName);

    List<String> packageComponents = nameComponents.subList(0, nameComponents.size() - 1);
    String className = nameComponents.get(nameComponents.size() - 1);

    var value = new NameWithMutablePackage(className, packageComponents);
    identifiers.put(qualifiedName, value);
  }

  /**
   * Get the ABI name for the type.
   *
   * @param type the type to look up the name for
   * @return the ABI name
   */
  String getAbiName(AstType type) {
    return getAbiName(type.asTypeElementUnsafe());
  }

  /**
   * Get the ABI name for the element.
   *
   * @param element the element to look up the name for
   * @return the ABI name
   */
  String getAbiName(Element element) {
    if (element.getKind() == ElementKind.CLASS) {
      TypeElement typeElement = (TypeElement) element;
      return lookupShortestNonClashingName(typeElement.getQualifiedName().toString());
    } else {
      String name = getFullyQualifiedName(element);
      return lookupShortestNonClashingName(name);
    }
  }

  /**
   * Lookup the shortest possible transformation of the qualified name.
   *
   * @param fullyQualifiedName the fully qualified name
   * @return the shortest non-clashing name
   */
  private String lookupShortestNonClashingName(String fullyQualifiedName) {
    NameWithMutablePackage nameWithPackage = identifiers.get(fullyQualifiedName);
    return nameWithPackage.reconstructWithPackage();
  }

  /**
   * Disambiguate all registered names into their simplest possible form that does not cause name
   * collisions.
   *
   * <p>For every clashing name increment the package delimiter index so e.g. a.b.C goes from C to
   * b.C.
   */
  void disambiguate() {
    for (List<NameWithMutablePackage> clashingNames : buildListOfClashingNames()) {
      clashingNames.forEach(NameWithMutablePackage::incrementPackagePointer);
    }

    if (!buildListOfClashingNames().isEmpty()) {
      disambiguate();
    }
  }

  /**
   * Create a map from the unique, shortened ABI name to the fully qualified name of the type.
   *
   * @return the map
   */
  Map<String, String> abiNameToQualified() {
    var result = new HashMap<String, String>();
    for (Map.Entry<String, NameWithMutablePackage> entry : identifiers.entrySet()) {
      result.put(entry.getValue().reconstructWithPackage(), entry.getKey());
    }
    return result;
  }

  /**
   * Build a list of type name that clash when shortened.
   *
   * @return a list of clashing names
   */
  private Collection<List<NameWithMutablePackage>> buildListOfClashingNames() {
    return identifiers.values().stream()
        .collect(Collectors.groupingBy(NameWithMutablePackage::reconstructWithPackage))
        .values()
        .stream()
        .filter(list -> list.size() > 1)
        .toList();
  }

  private List<String> splitToString(String packageName) {
    return Arrays.stream(packageName.split("\\.", -1)).toList();
  }

  List<String> getStructOrEnumNames() {
    var result = new ArrayList<String>();
    for (String name : structOrEnumNames) {
      result.add(identifiers.get(name).reconstructWithPackage());
    }
    return result;
  }

  List<Element> getElements() {
    return new ArrayList<>(elements.values());
  }

  /**
   * This represents a Java-like type name that consists of a package and a simple name with methods
   * to cut the package at a position.
   */
  static final class NameWithMutablePackage {

    private final String simpleName;
    private final List<String> packageComponents;

    private int pointer;

    NameWithMutablePackage(String simpleName, List<String> packageComponents) {
      this.simpleName = simpleName;
      this.packageComponents = packageComponents;
      this.pointer = 0;
    }

    /**
     * Reconstruct the name with the package components cut at the pointer.
     *
     * @return the reconstructed name
     */
    String reconstructWithPackage() {
      int fromIndex = packageComponents.size() - pointer;
      int toIndex = packageComponents.size();

      List<String> nameComponents = new ArrayList<>(packageComponents.subList(fromIndex, toIndex));
      nameComponents.add(simpleName);

      return String.join("_", nameComponents);
    }

    /**
     * Increment the package pointer.
     *
     * @return whether the pointer was changed
     */
    boolean incrementPackagePointer() {
      int oldPointer = pointer;
      pointer = Math.min(pointer + 1, packageComponents.size());
      return oldPointer != pointer;
    }
  }
}
