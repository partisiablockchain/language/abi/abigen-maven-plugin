package codegen;

import com.partisiablockchain.contract.CallbackContext;
import com.partisiablockchain.contract.reflect.Action;
import com.partisiablockchain.contract.reflect.AutoSysContract;
import com.partisiablockchain.contract.reflect.Callback;
import com.partisiablockchain.contract.reflect.Init;
import com.partisiablockchain.contract.reflect.Upgrade;
import com.partisiablockchain.contract.sys.SysContractContext;
import com.partisiablockchain.serialization.StateAccessor;
import com.partisiablockchain.serialization.StateVoid;
import com.secata.stream.SafeDataInputStream;

@AutoSysContract(StateVoid.class)
public final class CallbackAndActionSuccess {

  @Action(1)
  public StateVoid action1(int i1, long l1) {
    return null;
  }

  @Callback(0)
  public StateVoid callbackNoContext(int i1, long l1) {
    return null;
  }

  @Callback(2)
  public StateVoid callbackWithSysContext(SysContractContext context, int i1, long l1) {
    return null;
  }

  @Callback(3)
  public StateVoid callbackWithSysContextAndState(SysContractContext context, StateVoid state,
      int i1, long l1) {
    return null;
  }

  @Callback(4)
  public StateVoid callbackAll(SysContractContext context,
      StateVoid state,
      CallbackContext callbackContext,
      int i1,
      long l1) {
    return null;
  }

  @Callback(5)
  public StateVoid callbackAllWithRpc(SysContractContext context,
      StateVoid state,
      CallbackContext callbackContext,
      int i1,
      long l1,
      SafeDataInputStream rpc) {
    return null;
  }

  @Init
  public StateVoid init(int x, int y) {
    return null;
  }

  @Upgrade
  public StateVoid upgrade(StateAccessor stateAccessor) {
    return null;
  }
}
